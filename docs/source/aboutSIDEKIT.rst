.. _my-about:

About SIDEKIT
=============

    :Authors:
            Anthony Larcher \& Kong Aik Lee \& Sylvain Meignier
                         
    :Version: 1.0 of 2014/10/29

To know about the version and license of **SIDEKIT** ::

   sidekit.__version__
   sidekit.__license__

.. toctree::
   :maxdepth: 2

   contact.rst
   license.rst
   compatibilities.rst
