External links
==============

A non-exhaustive list of links that are of interest to the authors.

   * Tools for speaker recognition
      
      - `ALIZE <http://alize.univ-avignon.fr>`_ a speaker verification toolkit in C++
      - `BOSARIS <https://sites.google.com/site/bosaristoolkit/>`_ MATLAB code for calibration, fusion and evaluation of binary classifiers
      - `Focal <https://sites.google.com/site/nikobrummer/focal>`_ MATLAB code for evaluation, calibration and fusion of statistical pattern recognizers 
      - `HTK <http://htk.eng.cam.ac.uk>`_ portable toolkit for building and manipulating hidden Markov models in C++
      - `LIBSVM <http://www.csie.ntu.edu.tw/~cjlin/libsvm/>`_ a library for Support Vector Machines in C

   * Tools for Python programming
      
      - `h5py <http://www.h5py.org>`_ HDF5 library for Python
      - `Matplotlib <http://matplotlib.org>`_ 2D plotting library for Python
      - `Numpy <www.numpy.org>`_ package for scientific computing in Python 
      - `Pandas <http://pandas.pydata.org>`_ library to manage and analyse data
      - `Python <https://www.python.org>`_ official website
      - `Scipy <http://www.scipy.org>`_ python-based-ecosystem of software for mathematics, science and engineering
      - `Sphinx <http://sphinx-doc.org/>`_ tool to create easy documentation (including this very one) 
      - `Spyder <https://code.google.com/p/spyderlib/>`_ IDE for Python that allows line by line execution
      - `VirtualEnv <http://virtualenv.readthedocs.org/en/latest/>`_ to create isolated Python environments
   
   * Others programming tools

      - `HDF5 <http://www.hdfgroup.org/HDF5/>`_ a multi-platform library, and file format for storing and managing data

   * Speech resources for speaker recognition

      - `ELRA <http://www.elra.info/>`_
      - `LDC <http://www.ldc.upenn.edu/>`_ catalog
      - `NIST-Speaker Recognition Evaluation <http://www.csie.ntu.edu.tw/~cjlin/libsvm/>`_ main page
      - `RSR2015 <https://www.etpl.sg/innovation-offerings/ready-to-sign-licenses/rsr2015-overview-n-specifications>`_ a database for text-dependent speaker verification [Larcher14]_
     

More links can be find on the `ISCA <http://www.isca-speech.org/iscaweb/>`_ webpage.
