Additional modules
==================

.. toctree:: 
   :maxdepth: 2

   sidekit_io.rst
   iv_scoring.rst
   gmm_scoring.rst
   sv_utils.rst
   svm_training.rst
   svm_scoring.rst
   theano_utils.rst
