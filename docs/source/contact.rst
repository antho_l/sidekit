Contact
=======

The SIDEKIT project aims at enabling exchanges, contacts and collaborations between
academics and industrial actors in biometrics. You can get help, information and tips
via two channels

- a developers / users mailing list
- the responsibles of the projects

Mailing List
************

By subscribing to the `Dev-sidekit@keaton.univ-lemans.fr` mailing list
you will be able to post and receive information with othe users and developers
of SIDEKIT.

To post to this list, send your email to:

   dev-sidekit@keaton.univ-lemans.fr

General information about the mailing list is at:

   http://keaton.univ-lemans.fr/cgi-bin/mailman/listinfo/dev-sidekit

If you ever want to unsubscribe or change your options (eg, switch to
or from digest mode, change your password, etc.), visit your
subscription page at:

   http://keaton.univ-lemans.fr/cgi-bin/mailman/options/dev-sidekit/anthony.larcher%40univ-lemans.fr


You can also make such adjustments via email by sending a message to:

   Dev-sidekit-request@keaton.univ-lemans.fr

with the word `help` in the subject or body (don't include the
quotes), and you will get back a message with instructions.

You must know your password to change your options (including changing
the password, itself) or to unsubscribe.  It is:


Responsibles
************

| **Anthony Larcher**
|
| Associate Professor
| LIUM, Universite du Mans (FRANCE)
| anthony.larcher@univ-lemans.fr


| **Kong Aik Lee**
|
| Scientist
| Human and Language Technology, Institute for Infocomm Research, A*Star (SINGAPORE)
| kalee@i2r.a-star.edu.sg

| **Sylvain Meignier**
|
| Associate Professor
| LIUM, Universite du Mans (FRANCE)
| sylvain.meignier@univ-lemans.fr
