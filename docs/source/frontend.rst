The **frontend** package
========================

.. automodule:: frontend

| The :mod:`frontend` packgae provides tools to extract, normalize and select
| acoustic feature frames for speaker recognition. This package includes 
| 4 modules, each dedicated to a different step of the process.

.. toctree::
   :maxdepth: 2

   frontend/features.rst
   frontend/io.rst
   frontend/normfeat.rst
   frontend/vad.rst
