Main Classes
============

**SIDEKIT** is based on three main classes that are described below.

.. toctree::
   :maxdepth: 2
   
   featuresserver.rst
   mixture.rst
   statserver.rst
