NIST-SRE 2010
=============

| NIST Speaker Recognition Evaluation have been organized by the
| National Institute for Standard and Technologies (US) since 1997.
| Those evaluation have become a standard amongst the scientific community
| and are well used to develop and evaluate the speaker recognition systems.
| The proposed tutorials are based on the NIST-SRE 2010 extended protocol and makes use 
| of previous evaluation data to train the systems (NIST-SRE 2004, 2005, 2006 and 2008)
|
| More information can be found on `NIST-SRE website <http://www.nist.gov/itl/iad/mig/spkr-lang.cfm>`_.

The complete tutorial including Python scripts and metadata can be downloaded :download:`here <sidekit_sre10.tar.gz>`

.. toctree::
   :maxdepth: 2

   sre10_init.rst
   sre10_iv.rst
