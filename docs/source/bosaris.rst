The **bosaris** package
=======================



.. automodule:: bosaris


Content
-------

The Python bosaris package released as part of the ** sidekit** contains six modules detailed below. 

.. toctree::
   :maxdepth: 2

   bosaris/license.rst
   bosaris/detplot.rst
   bosaris/idmap.rst
   bosaris/key.rst
   bosaris/ndx.rst
   bosaris/plotwindow.rst
   bosaris/scores.rst
