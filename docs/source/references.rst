References
==========

.. [Bimbot04] Frédéric Bimbot, Jean-François Bonastre, Corinne Fredouille, Guillaume Gravier, Ivan Magrin-Chagnolleau, Sylvain Meignier, Teva Merlin, Javier Ortega-García, Dijana Petrovska-Delacrétaz, and Douglas A. Reynolds. **A tutorial on text-independent speaker verification.** *EURASIP journal on applied signal processing* (2004): 430-451.

.. [Bousquet11] Pierre-Michel Bousquet, Anthony Larcher, Driss Matrouf, Jean-Francois Bonastre and Ma Bin, **Application of new i-vector conditioning algorithm and scoring method to NIST speaker recognition evaluation 2010**, *in NIST Speaker Recognition Evaluation Analysis Workshop*, 2011

.. [Chang11] Chih-Chung Chang and Chih-Jen Lin, **LIBSVM : a library for support vector machines**, *in ACM Transactions on Intelligent Systems and Technology*, 2:27:1--27:27, 2011. Software available at http://www.csie.ntu.edu.tw/~cjlin/libsvm

.. [Cumani13] Sandro Cumani and Pietro Laface. **Fast and memory effective i-vector extraction using a factorized sub-space.**, INTERSPEECH, pages 1599-1603, 2013.

.. [Davis80] S.B. Davis and P. Mermelstein, **Comparison of parametric representations for monosyllabic word recognition in continuously spoken sentences**, *in IEEE Transaction on Acoustic,. Speech and Signal Processing*, TASSP-28 (4): 357-366, August 1980

.. [Garcia-Romero11] Daniel Garcia-Romero and C.Y. Espy-Wilson, **Analysis of i-vector length normalization in speaker recognition systems**, *in Annual Conference of the International Speech Communication Association (Interspeech)*, 2011, pp. 249-252

.. [Glembeck09] Ondrej Glembeck, Lukas Burget, Pavel Matejka, M. Karafiat and Patrick Kenny, **Simplification and optimization of I-Vector extraction**, *in IEEE International Conference on Acoustics, Speech, and Signal Processing, ICASSP*, 2011, 4516-4519

.. [Larcher14] Anthony Larcher, Kong Aik Lee, Bin Ma and Haizhou Li, **Text-dependent speaker verification: Classifiers, databases and RSR2015.**, Speech Communication 60 (2014): 56-77.

.. [Lee13] Kong Aik Lee, Anthony Larcher, Chang Huai You, Bin Ma and Haizhou Li, **Multi-session PLDA scoring of i-vector for partially open-set speaker detection**, In INTERSPEECH, 3651-3655, 2013