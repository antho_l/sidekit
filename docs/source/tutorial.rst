Start with a tutorial
=====================

In this section, you will find short tutorials on how to use the different components of the toolkt individually
and longer complete tutorial to train and run a speaker verification system on standard tasks such as the RSR2015 
database or the NIST Speaker Recognition Evaluation.

.. toctree::
   :maxdepth: 2
   
   tutorial/RSR2015.rst
   tutorial/SRE10.rst
   tutorial/SRE10_DNN.rst

