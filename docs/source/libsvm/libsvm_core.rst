LIBSVM Core library
===================


svm
---
.. automodule:: libsvm.svm
   :members:

svmutil
-------
.. automodule:: libsvm.svmutil
   :members:
