sv_utils
========


| This module provides miscellaneous tools that might be of use
| to the different parts of a speaker recognition engine.

.. automodule:: sv_utils
   :members:
